package com.huylv.marvelcomicsdemo.activity

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.huylv.marvelcomicsdemo.R
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    companion object {
        val PARAM_TYPE_VIEW = "PARAM_TYPE_VIEW"
        val INTENT_LIST_VIEW = 0
        val INTENT_GRID_VIEW = 1
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
        val intent = Intent(this, CharacterActivity::class.java)
        bt_list.setOnClickListener {
            intent.putExtra(PARAM_TYPE_VIEW, INTENT_LIST_VIEW)
            startActivity(intent)
        }
        bt_grid.setOnClickListener {
            intent.putExtra(PARAM_TYPE_VIEW, INTENT_GRID_VIEW)
            startActivity(intent)
        }
    }
}
