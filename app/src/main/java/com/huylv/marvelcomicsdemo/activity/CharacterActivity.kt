package com.huylv.marvelcomicsdemo.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.huylv.marvelcomicsdemo.R
import com.huylv.marvelcomicsdemo.view.fragment.CharacterFragment

class CharacterActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_character)
        loadFragment(CharacterFragment(), false)
    }

    public fun loadFragment(fragment: Fragment, isAddBackStack: Boolean) {
        val ft = supportFragmentManager.beginTransaction()
        if (isAddBackStack) {
            supportFragmentManager.popBackStack(
                fragment::class.java.simpleName,
                FragmentManager.POP_BACK_STACK_INCLUSIVE
            )
            ft.addToBackStack(fragment::class.java.simpleName)
        }
        ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left)
            .replace(R.id.fl_container, fragment, fragment::class.java.simpleName).commit()
    }

    override fun onBackPressed() {
        var fragment = supportFragmentManager.findFragmentByTag(CharacterFragment::class.java.simpleName)
        if (fragment is CharacterFragment && fragment.isVisible) {
            finish()
        }
        super.onBackPressed()
    }
}