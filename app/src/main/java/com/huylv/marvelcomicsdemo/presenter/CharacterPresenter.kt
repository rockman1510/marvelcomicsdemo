package com.huylv.marvelcomicsdemo.presenter

import android.util.Log
import com.huylv.marvelcomicsdemo.controller.ApiRepository
import com.huylv.marvelcomicsdemo.model.CharacterObject
import com.huylv.marvelcomicsdemo.model.CharacterRes
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers

class CharacterPresenter(
    private val apiRepository: ApiRepository,
    private val presenterView: PresenterView<CharacterObject>
) {

    private val TAG = CharacterPresenter::class.java.simpleName

    fun callCharactersApi(ts: Int, limit: Int, offset: Int, apiKey: String?, hash: String?) {
        Log.d(TAG, "callCharactersApi")
        apiRepository.getListCharacter(ts, limit, offset, apiKey, hash)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribeWith(object : DisposableObserver<CharacterRes>() {
                override fun onComplete() {
                    Log.d(TAG, "onComplete ")
                }

                override fun onError(e: Throwable?) {
                    Log.d(TAG, "onError: ${e!!.printStackTrace()}")
                    presenterView.onFailed("Invalid Error!")
                }

                override fun onNext(value: CharacterRes?) {
                    Log.d(TAG, "onNext - code: ${value!!.code}")
                    if (value!!.code == 200) {
                        presenterView.onSuccess(value.data.offset, value.data.results)
                    } else {
                        presenterView.onFailed(value.message)
                    }
                }
            })
    }

    private fun callSearchCharacterApi(ts: Int, name: String?, apiKey: String?, hash: String?) {
        Log.d(TAG, "callSearchCharacterApi: ")
        apiRepository.searchCharacter(ts, name, apiKey, hash)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribeWith(object : DisposableObserver<CharacterRes>() {
                override fun onComplete() {
                    Log.d(TAG, "onComplete")
                }

                override fun onError(e: Throwable?) {
                    presenterView.onFailed("Invalid Error!")
                    Log.d(TAG, "onError: ${e!!.printStackTrace()}")
                }

                override fun onNext(value: CharacterRes?) {
                    Log.d(TAG, "onNext - code: ${value!!.code}")
                    if (value!!.code == 200) {
                        presenterView.onSuccess(value.data.offset, value.data.results)
                    } else {
                        presenterView.onFailed(value.message)
                    }
                }
            })
    }

    fun searchCharacter(ts: Int, query: String, apiKey: String?, hash: String?, dataList: ArrayList<CharacterObject>) {
        var searchList = ArrayList<CharacterObject>()
        var isContained = false
        dataList.forEach { character ->
            if (character.name.contains(query, true)) {
                searchList.add(character)
                isContained = true
            }
        }
        if (!isContained) {
            callSearchCharacterApi(ts, query, apiKey, hash)
        } else {
            presenterView.onSuccess(0, searchList)
        }
    }


}