package com.huylv.marvelcomicsdemo.presenter

interface PresenterView<T> {
    fun onSuccess(offset: Int, data: List<T>)
    fun onFailed(message: String)
}