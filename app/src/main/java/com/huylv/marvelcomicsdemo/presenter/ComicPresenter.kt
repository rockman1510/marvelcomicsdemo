package com.huylv.marvelcomicsdemo.presenter

import android.util.Log
import com.huylv.marvelcomicsdemo.controller.ApiRepository
import com.huylv.marvelcomicsdemo.model.ComicObject
import com.huylv.marvelcomicsdemo.model.ComicRes
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers

class ComicPresenter(private val apiRepository: ApiRepository, private val presenterView: PresenterView<ComicObject>) {

    private val TAG = ComicPresenter::class.java.simpleName

    fun callComicsByCharacterIdApi(characterId: Int, ts: Int, apiKey: String?, hash: String?) {
        Log.d(TAG, "callComicsByCharacterIdApi")
        apiRepository.getComicsByCharacterId(characterId, ts, apiKey, hash)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribeWith(object : DisposableObserver<ComicRes>() {
                override fun onComplete() {
                    Log.d(TAG, "onComplete")
                }

                override fun onError(e: Throwable?) {
                    presenterView.onFailed("Invalid Error!")
                    Log.d(TAG, "onError: ${e!!.printStackTrace()}")
                }

                override fun onNext(value: ComicRes?) {
                    Log.d(TAG, "onNext - code: ${value!!.code}")
                    if (value!!.code == 200) {
                        presenterView.onSuccess(value.data.offset, value.data.results)
                    } else {
                        presenterView.onFailed(value.message)
                    }
                }
            })
    }
}