package com.huylv.marvelcomicsdemo.controller

class Constants {

    companion object {
        const val URL_ADDRESS = "http://gateway.marvel.com/v1/public/"
        const val TS_VALUE = 1
        const val API_KEY_VALUE = "f4fbe4b727f42c6d258e387557a6a5e3"
        const val HASH_VALUE = "e21a915eb659b305252bbc62c1c7b597"
        const val PARAM_COMICS = "/comics"
        const val PARAM_TS = "ts"
        const val PARAM_APIKEY = "apikey"
        const val PARAM_HASH = "hash"
        const val PARAM_LIMIT = "limit"
        const val PARAM_NAME = "name"
        const val PARAM_OFFSET = "offset"
        const val CODE = "code"
        const val MESSAGE = "message"
        const val STATUS = "status"
        const val DATA = "data"
        const val OFFSET = "offset"
        const val LIMIT = "limit"
        const val TOTAL = "total"
        const val COUNT = "count"
        const val RESULTS = "results"
        const val ID = "id"
        const val NAME = "name"
        const val DESCRIPTION = "description"
        const val THUMBNAIL = "thumbnail"
        const val PATH = "path"
        const val EXTENSION = "extension"
        const val COMICS = "comics"
        const val TITLE = "title"
        const val PRICES = "prices"
        const val TYPE = "type"
        const val PRICE = "price"
    }
}