package com.huylv.marvelcomicsdemo.controller

import com.huylv.marvelcomicsdemo.model.CharacterRes
import com.huylv.marvelcomicsdemo.model.ComicRes
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {

    @GET("characters?")
    fun listCharacterApi(
        @Query(Constants.PARAM_TS) ts: Int,
        @Query(Constants.PARAM_LIMIT) limit: Int,
        @Query(Constants.PARAM_OFFSET) offset: Int,
        @Query(Constants.PARAM_APIKEY) apiKey: String?,
        @Query(Constants.PARAM_HASH) hash: String?
    ): Observable<CharacterRes>

    @GET("characters?")
    fun characterByNameApi(
        @Query(Constants.PARAM_TS) ts: Int,
        @Query(Constants.PARAM_NAME) name: String?,
        @Query(Constants.PARAM_APIKEY) apiKey: String?,
        @Query(Constants.PARAM_HASH) hash: String?
    ): Observable<CharacterRes>


    @GET("characters/{characterId}" + Constants.PARAM_COMICS)
    fun comicsByCharacterIdApi(
        @Path("characterId") characterId: Int,
        @Query(Constants.PARAM_TS) ts: Int,
        @Query(Constants.PARAM_APIKEY) apiKey: String?,
        @Query(Constants.PARAM_HASH) hash: String?
    ): Observable<ComicRes>

    companion object Factory {
        fun create(): ApiService {
            val retrofit = retrofit2.Retrofit.Builder()
                .addCallAdapterFactory(retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory.create())
                .addConverterFactory(retrofit2.converter.gson.GsonConverterFactory.create())
                .baseUrl(Constants.URL_ADDRESS)
                .build()
            return retrofit.create(ApiService::class.java)
        }
    }
}