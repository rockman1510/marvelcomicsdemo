package com.huylv.marvelcomicsdemo.controller

import com.huylv.marvelcomicsdemo.model.CharacterRes
import com.huylv.marvelcomicsdemo.model.ComicRes
import io.reactivex.Observable

class ApiRepository(private val service: ApiService) {

    fun getListCharacter(ts: Int, limit: Int, offset: Int, apiKey: String?, hash: String?): Observable<CharacterRes> {
        return service.listCharacterApi(ts, limit, offset, apiKey, hash)
    }

    fun searchCharacter(ts: Int, name: String?, apiKey: String?, hash: String?): Observable<CharacterRes> {
        return service.characterByNameApi(ts, name, apiKey, hash)
    }

    fun getComicsByCharacterId(characterId: Int, ts: Int, apiKey: String?, hash: String?): Observable<ComicRes> {
        return service.comicsByCharacterIdApi(characterId, ts, apiKey, hash)
    }
}