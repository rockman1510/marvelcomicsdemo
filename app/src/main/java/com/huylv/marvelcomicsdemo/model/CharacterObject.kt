package com.huylv.marvelcomicsdemo.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.huylv.marvelcomicsdemo.controller.Constants

class CharacterObject() : Parcelable {

    @SerializedName(Constants.ID)
    @Expose
    var id = 0

    @SerializedName(Constants.NAME)
    @Expose
    var name = ""

    @SerializedName(Constants.DESCRIPTION)
    @Expose
    var description = ""

    @SerializedName(Constants.THUMBNAIL)
    @Expose
    lateinit var thumbnail: ThumbnailObject

    constructor(parcel: Parcel) : this() {
        id = parcel.readInt()
        name = parcel.readString()
        description = parcel.readString()
        thumbnail = parcel.readValue(ThumbnailObject::class.java.classLoader) as ThumbnailObject
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(name)
        parcel.writeString(description)
        parcel.writeValue(thumbnail)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CharacterObject> {
        override fun createFromParcel(parcel: Parcel): CharacterObject {
            return CharacterObject(parcel)
        }

        override fun newArray(size: Int): Array<CharacterObject?> {
            return arrayOfNulls(size)
        }
    }
}