package com.huylv.marvelcomicsdemo.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.huylv.marvelcomicsdemo.controller.Constants

class PricesObject {

    @SerializedName(Constants.TYPE)
    @Expose
    var type = ""

    @SerializedName(Constants.PRICE)
    @Expose
    var price: Float = 0.0f
}
