package com.huylv.marvelcomicsdemo.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.huylv.marvelcomicsdemo.controller.Constants

class ThumbnailObject {
    @SerializedName(Constants.PATH)
    @Expose
    var path = ""

    @SerializedName(Constants.EXTENSION)
    @Expose
    var extension = ""
}
