package com.huylv.marvelcomicsdemo.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.huylv.marvelcomicsdemo.controller.Constants

class CharacterRes {

    @SerializedName(Constants.CODE)
    @Expose
    var code: Int = 0

    @SerializedName(Constants.MESSAGE)
    @Expose
    var message = ""

    @SerializedName(Constants.STATUS)
    @Expose
    var status = ""

    @SerializedName(Constants.DATA)
    @Expose
    lateinit var data: CharacterDataRes

}