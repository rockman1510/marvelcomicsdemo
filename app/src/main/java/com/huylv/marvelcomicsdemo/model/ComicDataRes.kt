package com.huylv.marvelcomicsdemo.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.huylv.marvelcomicsdemo.controller.Constants

class ComicDataRes{

    @SerializedName(Constants.OFFSET)
    @Expose
    var offset: Int = 0

    @SerializedName(Constants.LIMIT)
    @Expose
    var limit: Int = 0

    @SerializedName(Constants.TOTAL)
    @Expose
    var total: Int = 0

    @SerializedName(Constants.COUNT)
    @Expose
    var count: Int = 0

    @SerializedName(Constants.RESULTS)
    @Expose
    lateinit var results: List<ComicObject>
}