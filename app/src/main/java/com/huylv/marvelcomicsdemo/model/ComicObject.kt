package com.huylv.marvelcomicsdemo.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.huylv.marvelcomicsdemo.controller.Constants

class ComicObject() : Parcelable {
    @SerializedName(Constants.ID)
    @Expose
    var id: Int = 0

    @SerializedName(Constants.TITLE)
    @Expose
    var title = ""

    @SerializedName(Constants.DESCRIPTION)
    @Expose
    var description = ""

    @SerializedName(Constants.PRICES)
    @Expose
    lateinit var prices: ArrayList<PricesObject>

    @SerializedName(Constants.THUMBNAIL)
    @Expose
    lateinit var thumbnailObject: ThumbnailObject

    constructor(parcel: Parcel) : this() {
        id = parcel.readInt()
        title = parcel.readString()
        description = parcel.readString()
        prices = parcel.readArrayList(PricesObject::class.java.classLoader) as ArrayList<PricesObject>
        thumbnailObject = parcel.readValue(ThumbnailObject::class.java.classLoader) as ThumbnailObject
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(title)
        parcel.writeString(description)
        parcel.writeList(prices)
        parcel.writeValue(thumbnailObject)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ComicObject> {
        override fun createFromParcel(parcel: Parcel): ComicObject {
            return ComicObject(parcel)
        }

        override fun newArray(size: Int): Array<ComicObject?> {
            return arrayOfNulls(size)
        }
    }
}