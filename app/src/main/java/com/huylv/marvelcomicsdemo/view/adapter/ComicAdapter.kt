package com.huylv.marvelcomicsdemo.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.huylv.marvelcomicsdemo.R
import com.huylv.marvelcomicsdemo.model.ComicObject

class ComicAdapter(var context: Context, var dataList: ArrayList<ComicObject>) :
    RecyclerView.Adapter<ComicAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.list_items, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.animation = AnimationUtils.loadAnimation(context, R.anim.slide_in_left)
        holder.itemView.animation.duration = 500
        Glide.with(context)
            .load(dataList[position].thumbnailObject.path + "." + dataList[position].thumbnailObject.extension)
            .error(R.drawable.placeholder).into(holder.iv_item)
        holder.tv_name.text = dataList[position].title
        var price = ""
        if (dataList[position].prices.size >= 2) {
            price = dataList[position].prices[1].price.toString() + " €"
        } else {
            price = dataList[position].prices[0].price.toString() + " €"
        }
        holder.tv_price.text = price

        holder.tv_name.textSize = 14f
        holder.tv_price.textSize = 14f
    }

    fun updateData(data: ArrayList<ComicObject>) {
        dataList = data
        notifyDataSetChanged()
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var iv_item: ImageView
        var tv_name: TextView
        var tv_price: TextView

        init {
            iv_item = view.findViewById(R.id.iv_item)
            tv_name = view.findViewById(R.id.tv_name)
            tv_price = view.findViewById(R.id.tv_price)
            tv_price.visibility = View.VISIBLE
        }
    }
}