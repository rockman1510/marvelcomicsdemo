package com.huylv.marvelcomicsdemo.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.huylv.marvelcomicsdemo.R
import com.huylv.marvelcomicsdemo.activity.MainActivity
import com.huylv.marvelcomicsdemo.model.CharacterObject

class CharacterAdapter(var context: Context, var dataList: ArrayList<CharacterObject>, var viewType: Int) :
    RecyclerView.Adapter<CharacterAdapter.ViewHolder>() {

    var onCharacterListener: OnCharacterListener? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view: View
        if (viewType == MainActivity.INTENT_LIST_VIEW) {
            view = LayoutInflater.from(context).inflate(R.layout.list_items, parent, false)
        } else {
            view = LayoutInflater.from(context).inflate(R.layout.grid_items, parent, false)
        }
        return ViewHolder(view, viewType)
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    override fun getItemViewType(position: Int): Int {
        return viewType
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (viewType == MainActivity.INTENT_GRID_VIEW) {
            holder.itemView.animation = AnimationUtils.loadAnimation(context, R.anim.zoom_in)
        } else {
            holder.itemView.animation = AnimationUtils.loadAnimation(context, R.anim.slide_in_left)
        }
        holder.itemView.animation.duration = 500
        Glide.with(context).load(dataList[position].thumbnail.path + "." + dataList[position].thumbnail.extension)
            .error(R.drawable.placeholder).into(holder.iv_item!!)
        holder.tv_name!!.text = dataList[position].name

        holder.itemView.setOnClickListener { onCharacterListener!!.onSelectItem(position, dataList[position]) }
    }

    public fun updateData(data: ArrayList<CharacterObject>) {
        val lastId = dataList.size - 1
        data.forEach { characterObject ->
            dataList.add(characterObject)
        }
        if (lastId >= 0) {
            notifyItemRangeChanged(lastId, data.size)
        } else {
            notifyDataSetChanged()
        }
        for (i in 0 until dataList.size) {
        }
    }

    public fun reverseData() {
        if (!dataList.isEmpty()) {
            dataList.reverse()
            notifyDataSetChanged()
        }
    }

    fun setData(data: ArrayList<CharacterObject>) {
        dataList.clear()
        data.forEach { obj ->
            dataList.add(obj)
        }
        notifyDataSetChanged()
    }

    class ViewHolder(view: View, viewType: Int) : RecyclerView.ViewHolder(view) {
        var iv_item: ImageView? = null
        var tv_name: TextView? = null

        init {
            if (viewType == MainActivity.INTENT_GRID_VIEW) {
                iv_item = view.findViewById(R.id.iv_grid)
                tv_name = view.findViewById(R.id.tv_grid)
            } else if (viewType == MainActivity.INTENT_LIST_VIEW) {
                iv_item = view.findViewById(R.id.iv_item)
                tv_name = view.findViewById(R.id.tv_name)
            }
        }
    }

    interface OnCharacterListener {
        fun onSelectItem(position: Int, characterObject: CharacterObject)
    }
}