package com.huylv.marvelcomicsdemo.view

import android.app.AlertDialog
import android.app.Dialog
import android.app.ProgressDialog
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Matrix
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.huylv.marvelcomicsdemo.R


class Utils {
    companion object {
        val FLIP_VERTICAL = 1
        val FLIP_HORIZONTAL = 2
        fun flipBitmap(bitmap: Bitmap, type: Int): Bitmap? {
            val matrix = Matrix()
            if (type === FLIP_VERTICAL) {
                matrix.preScale(1.0f, -1.0f)
            } else if (type === FLIP_HORIZONTAL) {
                matrix.preScale(-1.0f, 1.0f)
            } else {
                return null
            }
            return Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true)
        }

        fun loadingDialog(context: Context): ProgressDialog {
            val dialog = ProgressDialog(context)
            dialog.isIndeterminate
            dialog.setCancelable(true)
            dialog.setMessage(context.getString(R.string.loading))
            return dialog
        }

        fun messageDialog(context: Context, message: String): Dialog {
            return AlertDialog.Builder(context).setMessage(message).setCancelable(true).create()
        }

        fun showKeyboard(content: Context, view: View) {
            val imm = content.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY)
            view.requestFocus()
        }

        fun hideKeyboard(context: Context, view: View) {
            val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

}
