package com.huylv.marvelcomicsdemo.view.fragment

import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.huylv.marvelcomicsdemo.R
import com.huylv.marvelcomicsdemo.controller.ApiRepository
import com.huylv.marvelcomicsdemo.controller.ApiService
import com.huylv.marvelcomicsdemo.controller.Constants
import com.huylv.marvelcomicsdemo.model.CharacterObject
import com.huylv.marvelcomicsdemo.model.ComicObject
import com.huylv.marvelcomicsdemo.presenter.ComicPresenter
import com.huylv.marvelcomicsdemo.presenter.PresenterView
import com.huylv.marvelcomicsdemo.view.Utils
import com.huylv.marvelcomicsdemo.view.adapter.ComicAdapter
import com.huylv.marvelcomicsdemo.view.adapter.ComicPagerAdapter
import kotlinx.android.synthetic.main.fragment_character_detail.*

class DetailCharacterFragment : Fragment(), PresenterView<ComicObject> {

    var characterObject: CharacterObject? = null
    lateinit var comicAdapter: ComicAdapter
    lateinit var comicPresenter: ComicPresenter
    lateinit var pagerAdapter: ComicPagerAdapter
    lateinit var progressBar: ProgressBar

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_character_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        characterObject = arguments!!.getParcelable<CharacterObject>(CharacterFragment.KEY_CHARACTER_OBJECT)
        initializeUI()
        initializePresenter()
    }

    private fun initializePresenter() {
        progressBar.visibility = View.VISIBLE
        comicPresenter = ComicPresenter(ApiRepository(ApiService.create()), this)
        comicPresenter.callComicsByCharacterIdApi(
            characterObject!!.id,
            Constants.TS_VALUE,
            Constants.API_KEY_VALUE,
            Constants.HASH_VALUE
        )
    }

    private fun initializeUI() {
        Glide.with(this).load(characterObject!!.thumbnail.path + "." + characterObject!!.thumbnail.extension)
            .error(R.drawable.placeholder).into(iv_character)
        val scrollView = ScrollView(activity)
        val mpLayoutParams =
            ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        scrollView.layoutParams = mpLayoutParams
        val tvDescription = TextView(activity)
        tvDescription.layoutParams = mpLayoutParams
        tvDescription.textSize = 18F
        tvDescription.setTextColor(resources.getColor(R.color.colorBlack))
        var description = resources.getString(R.string.no_description)
        if (!TextUtils.isEmpty(characterObject!!.description)) {
            description = characterObject!!.description
        }
        tvDescription.text = description

        tvDescription.setPadding(12, 12, 12, 12)
        scrollView.addView(tvDescription)

        val relativeLayout = RelativeLayout(activity!!)
        relativeLayout.layoutParams = mpLayoutParams
        progressBar = ProgressBar(activity!!, null, android.R.attr.progressBarStyleSmall)
        val pbParams = RelativeLayout.LayoutParams(100, 100)
        pbParams.addRule(RelativeLayout.CENTER_IN_PARENT)
        progressBar.layoutParams = pbParams
        progressBar.interpolator

        val comicRecyclerView = RecyclerView(activity!!)
        comicRecyclerView.layoutParams = mpLayoutParams
        comicRecyclerView.background = ColorDrawable(resources.getColor(R.color.colorBackground))
        comicRecyclerView.layoutManager = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
        comicRecyclerView.setHasFixedSize(true)
        relativeLayout.addView(comicRecyclerView)
        relativeLayout.addView(progressBar)

        comicAdapter = ComicAdapter(activity!!, ArrayList<ComicObject>())
        comicRecyclerView.adapter = comicAdapter

        pagerAdapter = ComicPagerAdapter()
        pagerAdapter.addView(scrollView)
        pagerAdapter.addView(relativeLayout)
        viewPager.adapter = pagerAdapter

        changeSelected(0)
        bt_description.setOnClickListener {
            changeSelected(0)
            viewPager.currentItem = 0
        }
        bt_comics.setOnClickListener {
            changeSelected(1)
            viewPager.currentItem = 1
        }

        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }

            override fun onPageSelected(position: Int) {
                changeSelected(position)
            }
        })
    }

    private fun changeSelected(id: Int) {
        if (id == 0) {
            bt_description.isSelected = true
            bt_comics.isSelected = false
        } else {
            bt_description.isSelected = false
            bt_comics.isSelected = true
        }
    }

    override fun onSuccess(offset: Int, data: List<ComicObject>) {
        progressBar.visibility = View.GONE
        if (data.isEmpty())
            Toast.makeText(activity, resources.getString(R.string.no_comics), Toast.LENGTH_LONG).show()
        comicAdapter.updateData(data as ArrayList<ComicObject>)
    }

    override fun onFailed(message: String) {
        Utils.messageDialog(activity!!, message).show()
    }

}