package com.huylv.marvelcomicsdemo.view.adapter

import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager

class ComicPagerAdapter : PagerAdapter() {

    var views: ArrayList<View>? = null

    init {
        if (views == null) {
            views = ArrayList()
        }
    }

    override fun isViewFromObject(view: View, obj: Any): Boolean {
        return view == obj
    }

    override fun getCount(): Int {
        return views!!.size
    }

    override fun getItemPosition(obj: Any): Int {
        return views!!.indexOf(obj)
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        var view = views!![position]
        container.addView(view)
        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(views!![position])
    }

    fun addView(v: View, position: Int) {
        views!![position] = v
    }

    fun addView(v: View) {
        views!!.add(v)
    }

    fun removeView(pager: ViewPager, view: View) {
        pager.adapter = null
        pager.removeViewAt(views!!.indexOf(view))
        pager.adapter = this
    }

    fun getView(position: Int): View {
        return views!![position]
    }

}