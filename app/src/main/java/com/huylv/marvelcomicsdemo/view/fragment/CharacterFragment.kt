package com.huylv.marvelcomicsdemo.view.fragment

import android.app.Dialog
import android.app.ProgressDialog
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.huylv.marvelcomicsdemo.R
import com.huylv.marvelcomicsdemo.activity.CharacterActivity
import com.huylv.marvelcomicsdemo.activity.MainActivity
import com.huylv.marvelcomicsdemo.controller.ApiRepository
import com.huylv.marvelcomicsdemo.controller.ApiService
import com.huylv.marvelcomicsdemo.controller.Constants
import com.huylv.marvelcomicsdemo.model.CharacterObject
import com.huylv.marvelcomicsdemo.presenter.CharacterPresenter
import com.huylv.marvelcomicsdemo.presenter.PresenterView
import com.huylv.marvelcomicsdemo.view.Utils
import com.huylv.marvelcomicsdemo.view.adapter.CharacterAdapter
import kotlinx.android.synthetic.main.fragment_characters.*

class CharacterFragment : Fragment(), CharacterAdapter.OnCharacterListener, PresenterView<CharacterObject>,
    SearchView.OnQueryTextListener {

    override fun onQueryTextSubmit(query: String?): Boolean {
        Log.d(TAG, "onQueryTextSubmit")
        Utils.hideKeyboard(activity!!, search_view)
        if (!TextUtils.isEmpty(query)) {
            Log.d(TAG, "query")
            isSearch = true
            loadingDialog.show()
            characterPresenter.searchCharacter(
                Constants.TS_VALUE,
                query!!,
                Constants.API_KEY_VALUE,
                Constants.HASH_VALUE,
                savedList
            )
        } else {
            adapter.setData(savedList)
            isSearch = false
        }
        return true
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        Log.d(TAG, "onQueryTextChange")
        if (!TextUtils.isEmpty(newText)) {
            isSearch = true
            Log.d(TAG, "newText")
            characterPresenter.searchCharacter(
                Constants.TS_VALUE,
                newText!!,
                Constants.API_KEY_VALUE,
                Constants.HASH_VALUE,
                savedList
            )
        } else {
            adapter.setData(savedList)
            isSearch = false
        }
        return true
    }

    val TAG = "CharacterFragment"

    companion object {
        public val KEY_CHARACTER_OBJECT = "KEY_CHARACTER_OBJECT"
    }

    private lateinit var adapter: CharacterAdapter
    private var viewType: Int = 0
    private val limit = 10
    private var offSet = 0
    private lateinit var characterPresenter: CharacterPresenter
    private lateinit var loadingDialog: ProgressDialog
    private lateinit var searchRecycleView: RecyclerView
    lateinit var savedList: ArrayList<CharacterObject>
    var isLoading = false
    var isSearch = false
    private lateinit var messagesDialog: Dialog

    override fun onSelectItem(position: Int, characterObject: CharacterObject) {
        var fragment = DetailCharacterFragment()
        val bundle = Bundle()
        bundle.putParcelable(KEY_CHARACTER_OBJECT, characterObject)
        fragment.arguments = bundle
        (activity as (CharacterActivity)).loadFragment(fragment, true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_characters, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewType = activity!!.intent.getIntExtra(MainActivity.PARAM_TYPE_VIEW, 0)
        initializeUI()
    }

    private fun updateData() {
        loadingDialog.show()
        characterPresenter = CharacterPresenter(ApiRepository(ApiService.create()), this)
        characterPresenter.callCharactersApi(
            Constants.TS_VALUE,
            limit,
            offSet,
            Constants.API_KEY_VALUE,
            Constants.HASH_VALUE
        )
    }

    override fun onResume() {
        super.onResume()
        offSet = 0
        updateData()
    }

    private fun initializeUI() {
        activity!!.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
        rv_character.setHasFixedSize(true)
        rv_character.itemAnimator = DefaultItemAnimator()
        if (viewType == MainActivity.INTENT_GRID_VIEW) {
            rv_character.layoutManager = GridLayoutManager(activity!!, 2) as RecyclerView.LayoutManager?
        } else {
            rv_character.layoutManager = LinearLayoutManager(activity!!, RecyclerView.VERTICAL, false)
        }
        savedList = ArrayList<CharacterObject>()
        adapter =
            CharacterAdapter(activity!!, ArrayList<CharacterObject>(), viewType)
        adapter.onCharacterListener = this
        rv_character.adapter = adapter
        isSearch = false
        search_view.queryHint = resources.getString(R.string.search)
        search_view.onActionViewExpanded()
        search_view.clearFocus()
        search_view.setOnQueryTextListener(this)

        rv_character.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (dy > 0) {
                    val visibleItem = rv_character.layoutManager!!.childCount
                    val totalItem = rv_character.layoutManager!!.itemCount
                    val pastVisibleItem =
                        (rv_character.layoutManager!! as LinearLayoutManager).findFirstVisibleItemPosition()
                    if (!isLoading && !isSearch && TextUtils.isEmpty(search_view.query.toString())) {
                        if (pastVisibleItem + visibleItem >= totalItem) {
                            Log.d(TAG, "reach bottom")
                            isLoading = true
                            updateData()
                        }
                    }
                }
            }
        })

        iv_filter.setColorFilter(resources.getColor(R.color.colorBlack))
        iv_filter.setOnClickListener {
            if (!adapter.dataList.isEmpty()) {
                if (adapter.dataList[0].id > adapter.dataList[adapter.dataList.size - 1].id) {
                    iv_filter.setColorFilter(resources.getColor(R.color.colorBlue))
                } else {
                    iv_filter.setColorFilter(resources.getColor(R.color.colorBlack))
                }
                adapter.reverseData()
            }
        }
        loadingDialog = Utils.loadingDialog(activity!!)
        messagesDialog = Utils.messageDialog(
            activity!!,
            resources.getString(R.string.has_no_result)
        )
    }

    override fun onSuccess(offSetValue: Int, data: List<CharacterObject>) {
        if (loadingDialog.isShowing)
            loadingDialog.dismiss()
        Log.d(TAG, "offSetValue: $offSetValue")
        if (isSearch) {
            Log.d(TAG, "onSuccess - search")
            if (data.isEmpty()) {
                if (!messagesDialog.isShowing)
                    messagesDialog.show()
            }
            adapter.setData(data as ArrayList<CharacterObject>)
        } else {
            Log.d(TAG, "onSuccess")
            offSet = offSetValue + 10
            adapter.updateData(data as ArrayList<CharacterObject>)
            savedList.clear()
            for (characterObject in adapter.dataList) {
                savedList.add(characterObject)
            }
            Log.d(TAG, "savedList.size: ${savedList.size}")
        }
        isLoading = false
    }

    override fun onFailed(message: String) {
        if (loadingDialog.isShowing)
            loadingDialog.dismiss()
        Utils.messageDialog(activity!!, message).show()
    }

}