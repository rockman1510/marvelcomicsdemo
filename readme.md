- That project i made structure MVP: Model - View - Presenter
    + Model to define data structure
    + View to display data from Presenter
    + Presenter to call API for getting data then hold it in Model,
- I made it with 2 activities:
    + MainActivity for main screen
    + CharacterActivity for character list screen and detail character screen
- 2 screens of character list and detail character made into 2 Fragment: CharacterFragment and DetailCharacterFragment
- Applying some animations in changing between activities, fragments, and view items
- All data will be pulled online from Marvel API
- Search box can search both offline and online mode (if offline data doesn't contain query value)
- Sort button can do increasing sort and decreasing sort of data
- Using Retrofit 2 library combine with RxJava to request and get data response from web service in Observer architecture
- Using Glide library to load image through and url
- Using CardView library to display view items
- Using RecyclerView library to display list and grid view
- Using ViewPager for tab description and comic in detail character screen